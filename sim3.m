% h01=0.5	MVG=1	MV3=1

close all;
clear all;
clc;

MVA = 0;
MVB = 0;
MVC = 0;
MVD = 0;
MVE = 0;
MVF = 0;
MVG = 1;
MV1 = 0;
MV2 = 0;
MV3 = 1;
MV4 = 0;

h01 = 0.5;
h02 = 0;
h03 = 0;
h04 = 0;

cnte_b1 = 0;
dt_b1 = 10;
cnte_b2 = 0;
dt_b2 = 10;

ts = 250;   % Tempo de simula��o

sim('Modelo.slx')

plot(tout,h1(:,2),'r','Linewidth',2);
hold on
plot(tout,h2(:,2),'b','Linewidth',2);
plot(tout,h3(:,2),'--k','Linewidth',2);
plot(tout,h4(:,2),'g','Linewidth',2);
grid on
title('N�veis dos tanques','FontSize',16)
legend('h1','h2','h3','h4','Location','northeast')
xlabel('t [s]','FontSize',16)
ylabel('N�veis [m]','FontSize',16)
set(gca,'FontSize',14)
hold off
axis([0 250 -0.05 0.55])