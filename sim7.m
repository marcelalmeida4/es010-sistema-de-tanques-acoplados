% b1=on	b2=on MVA=MVB=MVC=MVD=MVE=MVF=1 MV1=1

close all;
clear all;
clc;

MVA = 1;
MVB = 1;
MVC = 1;
MVD = 1;
MVE = 1;
MVF = 1;
MVG = 0;
MV1 = 1;
MV2 = 0;
MV3 = 0;
MV4 = 0;

h01 = 0;
h02 = 0;
h03 = 0;
h04 = 0;

cnte_b1 = 0.0001;
dt_b1 = 10;
cnte_b2 = 0.0001;
dt_b2 = 10;

ts = 110;   % Tempo de simula��o

sim('Modelo.slx')

plot(tout,h1(:,2),'r','Linewidth',2);
hold on
grid on
plot(tout,h2(:,2),'b','Linewidth',2);
plot(tout,h3(:,2),'--k','Linewidth',2);
plot(tout,h4(:,2),'--g','Linewidth',2);
title('N�veis dos tanques','FontSize',16)
legend('h1','h2','h3','h4','Location','northeast')
xlabel('t [s]','FontSize',16)
ylabel('N�veis [m]','FontSize',16)
set(gca,'FontSize',14)
hold off
axis([0 110 0 0.2])